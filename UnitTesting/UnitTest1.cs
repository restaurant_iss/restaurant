﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Collections.Generic;
using System.ComponentModel;
//using System;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UnitTesting
{
    [TestClass]
    public class UnitTest1
    {
        //public static String connectionString = "Data Source = VALENTINESDAY\\SQLEXPRESS; Initial Catalog = Restaurant_final; Integrated Security = true";
        //public static String connectionString = "Data Source = DESKTOP-CP0TKLA\\SQLEXPRESS01; Initial Catalog = Restaurant_final; Integrated Security=true";
        //public static String connectionString = "Data Source = DESKTOP-053SR5H\\SQLEXPRESS; Initial Catalog = Restaurant_final; Integrated Security = true";
        //public static String connectionString = "Data Source = ELA-PC\\SQLEXPRESS; Initial Catalog = Restaurant_final; Integrated Security = true";
        public static String connectionString = "Data Source = DESKTOP-LVOU8AT\\SQLEXPRESS; Initial Catalog = Restaurant_final; Integrated Security = true";
        //public static String connectionString = "Data Source=DIANA\\SQLEXPRESS; Initial Catalog=Restaurant_final; User Id=sa;Password=ceraa@me";
        //public static String connectionString = "Data Source = ALEXANDRA\\SQLEXPRESS; Initial Catalog = Restaurant_final; Integrated Security = true";
        SqlConnection con = new SqlConnection(connectionString);
        SqlDataAdapter da = new SqlDataAdapter();
        DataSet ds = new DataSet();

        [TestMethod]
        public void manageStaff_test_add()
        {
            da.InsertCommand = new SqlCommand("Insert into Employees(Name,Surname,Birthdate,CNP,Address"
                + ",EmploymentDate,Position,Salary,Observations,ProfileImg) " +
                "Values (@name,@surname,@birthDate,@cnp,@address,@emplDate,@position,@salary,@obs,@profileImg)", con);
            da.InsertCommand.Parameters.Add("@name", SqlDbType.NChar).Value = "Ana";
            da.InsertCommand.Parameters.Add("@surname", SqlDbType.NChar).Value = "Maria";
            da.InsertCommand.Parameters.Add("@birthDate", SqlDbType.Date).Value = "1996-02-03";
            da.InsertCommand.Parameters.Add("@cnp", SqlDbType.NChar).Value = "1234567890123";
            da.InsertCommand.Parameters.Add("@address", SqlDbType.NChar).Value = "abc";
            da.InsertCommand.Parameters.Add("@emplDate", SqlDbType.Date).Value = "2013-02-03";
            da.InsertCommand.Parameters.Add("@position", SqlDbType.NChar).Value = "cook";
            da.InsertCommand.Parameters.Add("@salary", SqlDbType.Float).Value = 200.5;
            da.InsertCommand.Parameters.Add("@obs", SqlDbType.NChar).Value = "abc";
            da.InsertCommand.Parameters.Add("@profileImg", SqlDbType.NChar).Value = "a";
            con.Open();
            int nr_of_rows = da.InsertCommand.ExecuteNonQuery();
            con.Close();
            Assert.AreEqual(nr_of_rows, 1);
        }

        [TestMethod]
        public void manageStaff_test_delete()
        {
            da.InsertCommand = new SqlCommand("Insert into Employees(Name,Surname,Birthdate,CNP,Address"
                + ",EmploymentDate,Position,Salary,Observations,ProfileImg) " +
                "Values (@name,@surname,@birthDate,@cnp,@address,@emplDate,@position,@salary,@obs,@profileImg)", con);
            da.InsertCommand.Parameters.Add("@name", SqlDbType.NChar).Value = "Anaa";
            da.InsertCommand.Parameters.Add("@surname", SqlDbType.NChar).Value = "Maria";
            da.InsertCommand.Parameters.Add("@birthDate", SqlDbType.Date).Value = "1996-02-03";
            da.InsertCommand.Parameters.Add("@cnp", SqlDbType.NChar).Value = "1234567890123";
            da.InsertCommand.Parameters.Add("@address", SqlDbType.NChar).Value = "abc";
            da.InsertCommand.Parameters.Add("@emplDate", SqlDbType.Date).Value = "2013-02-03";
            da.InsertCommand.Parameters.Add("@position", SqlDbType.NChar).Value = "cook";
            da.InsertCommand.Parameters.Add("@salary", SqlDbType.Float).Value = 200.5;
            da.InsertCommand.Parameters.Add("@obs", SqlDbType.NChar).Value = "abc";
            da.InsertCommand.Parameters.Add("@profileImg", SqlDbType.NChar).Value = "a";

            con.Open();
            da.InsertCommand.ExecuteNonQuery();
                
            SqlCommand cmd = new SqlCommand("Delete from Employees where Salary = 0", con);

            int nr_of_rows = cmd.ExecuteNonQuery();
            con.Close();
            Assert.AreEqual(nr_of_rows, 0);
        }

        [TestMethod]
        public void manageStaff_test_update()
        {
            da.InsertCommand = new SqlCommand("Insert into Employees(Name,Surname,Birthdate,CNP,Address"
                + ",EmploymentDate,Position,Salary,Observations,ProfileImg) " +
                "Values (@name,@surname,@birthDate,@cnp,@address,@emplDate,@position,@salary,@obs,@profileImg)", con);
            da.InsertCommand.Parameters.Add("@name", SqlDbType.NChar).Value = "Paul";
            da.InsertCommand.Parameters.Add("@surname", SqlDbType.NChar).Value = "Maria";
            da.InsertCommand.Parameters.Add("@birthDate", SqlDbType.Date).Value = "1996-02-03";
            da.InsertCommand.Parameters.Add("@cnp", SqlDbType.NChar).Value = "1234567890123";
            da.InsertCommand.Parameters.Add("@address", SqlDbType.NChar).Value = "abc";
            da.InsertCommand.Parameters.Add("@emplDate", SqlDbType.Date).Value = "2013-02-03";
            da.InsertCommand.Parameters.Add("@position", SqlDbType.NChar).Value = "cook";
            da.InsertCommand.Parameters.Add("@salary", SqlDbType.Float).Value = 200.5;
            da.InsertCommand.Parameters.Add("@obs", SqlDbType.NChar).Value = "abc";
            da.InsertCommand.Parameters.Add("@profileImg", SqlDbType.NChar).Value = "a";
            con.Open();
            da.InsertCommand.ExecuteNonQuery();
            SqlCommand cmd = new SqlCommand("UPDATE Employees SET Salary=300 where Salary = 0", con);

            int nr_of_rows = cmd.ExecuteNonQuery();
            con.Close();
            Assert.AreEqual(nr_of_rows, 0);
        }
    }
}
