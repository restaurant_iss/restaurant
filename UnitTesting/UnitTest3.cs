﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Data;
using System.Data.SqlClient;

namespace UnitTesting
{
    [TestClass]
    public class UnitTest3
    {
        //public static String connectionString = "Data Source = VALENTINESDAY\\SQLEXPRESS; Initial Catalog = Restaurant_final; Integrated Security = true";
        //public static String connectionString = "Data Source = DESKTOP-CP0TKLA\\SQLEXPRESS01; Initial Catalog = Restaurant_final; Integrated Security=true";
        //public static String connectionString = "Data Source = DESKTOP-053SR5H\\SQLEXPRESS; Initial Catalog = Restaurant_final; Integrated Security = true";
        //public static String connectionString = "Data Source = ELA-PC\\SQLEXPRESS; Initial Catalog = Restaurant_final; Integrated Security = true";
        public static String connectionString = "Data Source = DESKTOP-LVOU8AT\\SQLEXPRESS; Initial Catalog = Restaurant_final; Integrated Security = true";
        //public static String connectionString = "Data Source=DIANA\\SQLEXPRESS; Initial Catalog=Restaurant_final; User Id=sa;Password=ceraa@me";
        //public static String connectionString = "Data Source = ALEXANDRA\\SQLEXPRESS; Initial Catalog = Restaurant_final; Integrated Security = true";
        SqlConnection con = new SqlConnection(connectionString);
        SqlDataAdapter da = new SqlDataAdapter();
        DataSet ds = new DataSet();

        [TestMethod]
        public void updateMenus_test_add()
        {
            da.InsertCommand = new SqlCommand("Insert into Menu(Title_Menu,Description_Menu,Photo_Menu,Category_Menu,Price) Values (@title,@desc,@photo,@category,@price)", con);
            da.InsertCommand.Parameters.Add("@title", SqlDbType.NChar).Value = "ab";
            da.InsertCommand.Parameters.Add("@desc", SqlDbType.NChar).Value = "delicious";
            da.InsertCommand.Parameters.Add("@photo", SqlDbType.NChar).Value = "ph";
            da.InsertCommand.Parameters.Add("@category", SqlDbType.NChar).Value = "paste";
            da.InsertCommand.Parameters.Add("@price", SqlDbType.Float).Value = 30.5;
            con.Open();
            int nr_of_rows = da.InsertCommand.ExecuteNonQuery();
            con.Close();
            Assert.AreEqual(nr_of_rows, 1);
        }

        [TestMethod]
        public void updateMenus_test_update()
        {
            da.InsertCommand = new SqlCommand("Insert into Menu(Title_Menu,Description_Menu,Photo_Menu,Category_Menu,Price) Values (@title,@desc,@photo,@category,@price)", con);
            da.InsertCommand.Parameters.Add("@title", SqlDbType.NChar).Value = "ab";
            da.InsertCommand.Parameters.Add("@desc", SqlDbType.NChar).Value = "delicious";
            da.InsertCommand.Parameters.Add("@photo", SqlDbType.NChar).Value = "ph";
            da.InsertCommand.Parameters.Add("@category", SqlDbType.NChar).Value = "paste";
            da.InsertCommand.Parameters.Add("@price", SqlDbType.Float).Value = 30.5;
            con.Open();
            da.InsertCommand.ExecuteNonQuery();
            SqlCommand cmd = new SqlCommand("UPDATE Menu SET Price=300 where Price = 0", con);

            int nr_of_rows = cmd.ExecuteNonQuery();
            con.Close();
            Assert.AreEqual(nr_of_rows, 0);
        }

        [TestMethod]
        public void updateMenus_test_delete()
        {
            da.InsertCommand = new SqlCommand("Insert into Menu(Title_Menu,Description_Menu,Photo_Menu,Category_Menu,Price) Values (@title,@desc,@photo,@category,@price)", con);
            da.InsertCommand.Parameters.Add("@title", SqlDbType.NChar).Value = "ab";
            da.InsertCommand.Parameters.Add("@desc", SqlDbType.NChar).Value = "delicious";
            da.InsertCommand.Parameters.Add("@photo", SqlDbType.NChar).Value = "ph";
            da.InsertCommand.Parameters.Add("@category", SqlDbType.NChar).Value = "paste";
            da.InsertCommand.Parameters.Add("@price", SqlDbType.Float).Value = 30.5;
            con.Open();
            da.InsertCommand.ExecuteNonQuery();

            SqlCommand cmd = new SqlCommand("Delete from Menu where Price = 0", con);

            int nr_of_rows = cmd.ExecuteNonQuery();
            con.Close();
            Assert.AreEqual(nr_of_rows, 0);
        }
    }
}
