﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ISS_Application
{
    public partial class collectMoney : Form
    {
        SqlConnection connection = new SqlConnection(Program.connectionString);
        SqlDataAdapter da = new SqlDataAdapter();
        DataSet ds = new DataSet();
        BindingSource bs = new BindingSource();

        public collectMoney()
        {
            InitializeComponent();
        }

        private void collectMoney_Load(object sender, EventArgs e)
        {

            da.SelectCommand = new SqlCommand(@"SELECT * FROM Orders o
                                                INNER JOIN Menu m ON o.ID_Menu = m.ID_Menu
                                                WHERE Pay_Request = 1 AND Status <> 'done'", connection);
            da.Fill(ds, "OrdersToCollect");

            bs.DataSource = ds;
            bs.DataMember = "OrdersToCollect";
            dataGridViewPayRequests.AutoGenerateColumns = false;
            dataGridViewPayRequests.ColumnCount = 4;
            dataGridViewPayRequests.ReadOnly = true;

            dataGridViewPayRequests.Columns[0].Name = "Table";
            dataGridViewPayRequests.Columns[0].DataPropertyName = "TableNumber";

            dataGridViewPayRequests.Columns[1].Name = "Menu";
            dataGridViewPayRequests.Columns[1].DataPropertyName = "Title_Menu";

            dataGridViewPayRequests.Columns[2].Name = "Price";
            dataGridViewPayRequests.Columns[2].DataPropertyName = "Price";

            dataGridViewPayRequests.Columns[3].Name = "ID_Order";
            dataGridViewPayRequests.Columns[3].DataPropertyName = "ID_Order";
            dataGridViewPayRequests.Columns[3].Visible = false;

            dataGridViewPayRequests.DataSource = bs;
        }

        private void buttonBack_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void buttonPrice_Click(object sender, EventArgs e)
        {
            double totalSum = 0;
           
            foreach (DataGridViewRow r in dataGridViewPayRequests.SelectedRows)
            {
                double res;
                Double.TryParse(r.Cells["Price"].Value.ToString(), out res);
                totalSum += res;
            }
           
            labelComputedPrice.Text = totalSum.ToString();
        }

        private void buttonChange_Click(object sender, EventArgs e)
        {
            string input = textBoxReceivedSum.Text;

            if (input == "")
            {
                MessageBox.Show("Please input a sum of money.");
                return;
            }
               

            double inputSum, total = Double.Parse(labelComputedPrice.Text.ToString());
            try
            {
                inputSum = Double.Parse(input);
                
                if(inputSum < total)
                {
                    MessageBox.Show("Please input a sum greater or equal than the total price");
                }
                else
                {
                    labelComputed.Text = (inputSum - total).ToString();
                }
                
            }
            catch(FormatException ex)
            {
                MessageBox.Show("Please input a valid sum of money");
            }
            
        }

        private void buttonDone_Click(object sender, EventArgs e)
        {
            connection.Open();
            SqlCommand cmd;
            foreach (DataGridViewRow r in dataGridViewPayRequests.SelectedRows)
            {
                int id;
                int.TryParse(r.Cells["ID_Order"].Value.ToString(), out id);
                cmd = new SqlCommand("DELETE FROM Orders WHERE ID_Order = @id");
                cmd.CommandType = CommandType.Text;
                cmd.Parameters.Add("@id", SqlDbType.Int).Value = id;
                cmd.Connection = connection;
                cmd.ExecuteNonQuery();
            }
            
            ds.Tables["OrdersToCollect"].Clear();
            da.Fill(ds, "OrdersToCollect");
            dataGridViewPayRequests.Update();
            dataGridViewPayRequests.Refresh();
            connection.Close();
            
            
        }
    }
}
