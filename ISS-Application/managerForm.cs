﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ISS_Application
{
    public partial class managerForm : Form
    {
        manageStaff manageStaffInstance = null;
        manageReports manageReportsInstance = null;
        updateMenus updateMenusInstance = null;
        supplyIngredients supplyIngrForm = null;
        public managerForm()
        {
            InitializeComponent();
        }

        private void btnManageStaff_Click(object sender, EventArgs e)
        {
            
            manageStaffInstance = new manageStaff();
            manageStaffInstance.Show();
        }

        private void btnReports_Click(object sender, EventArgs e)
        {
            manageReportsInstance = new manageReports();
            manageReportsInstance.Show();
        }

        private void managerForm_Load(object sender, EventArgs e)
        {

        }

        private void btnMenus_Click(object sender, EventArgs e)
        {
            updateMenusInstance = new updateMenus();
            updateMenusInstance.Show();
        }

        private void btnSupply_Click(object sender, EventArgs e)
        {
            supplyIngrForm = new supplyIngredients();
            supplyIngrForm.Show();
        }

        private void buttonLogout_Click(object sender, EventArgs e)
        {
            this.Hide();
            MainForm m = new MainForm();
            m.FormClosed += (s, a) => this.Close();
            m.Show();
        }
    }
}
