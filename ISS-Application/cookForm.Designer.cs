﻿namespace ISS_Application
{
    partial class cookForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.labelTitle = new System.Windows.Forms.Label();
            this.btnViewIngredients = new System.Windows.Forms.Button();
            this.btnRequestIngredients = new System.Windows.Forms.Button();
            this.buttonLogout = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // labelTitle
            // 
            this.labelTitle.AutoSize = true;
            this.labelTitle.BackColor = System.Drawing.Color.Transparent;
            this.labelTitle.Font = new System.Drawing.Font("Monotype Corsiva", 27.75F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelTitle.ForeColor = System.Drawing.Color.SaddleBrown;
            this.labelTitle.Location = new System.Drawing.Point(528, 33);
            this.labelTitle.Name = "labelTitle";
            this.labelTitle.Size = new System.Drawing.Size(437, 45);
            this.labelTitle.TabIndex = 14;
            this.labelTitle.Text = "Welcome to the Cook\'s Menu!";
            // 
            // btnViewIngredients
            // 
            this.btnViewIngredients.BackColor = System.Drawing.Color.PapayaWhip;
            this.btnViewIngredients.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnViewIngredients.Font = new System.Drawing.Font("Papyrus", 11.25F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnViewIngredients.Location = new System.Drawing.Point(340, 344);
            this.btnViewIngredients.Name = "btnViewIngredients";
            this.btnViewIngredients.Size = new System.Drawing.Size(202, 54);
            this.btnViewIngredients.TabIndex = 15;
            this.btnViewIngredients.Text = "View ingredients";
            this.btnViewIngredients.UseVisualStyleBackColor = false;
            this.btnViewIngredients.Click += new System.EventHandler(this.btnViewIngredients_Click);
            // 
            // btnRequestIngredients
            // 
            this.btnRequestIngredients.BackColor = System.Drawing.Color.PapayaWhip;
            this.btnRequestIngredients.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnRequestIngredients.Font = new System.Drawing.Font("Papyrus", 11.25F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnRequestIngredients.Location = new System.Drawing.Point(777, 344);
            this.btnRequestIngredients.Name = "btnRequestIngredients";
            this.btnRequestIngredients.Size = new System.Drawing.Size(202, 54);
            this.btnRequestIngredients.TabIndex = 16;
            this.btnRequestIngredients.Text = "Request ingredients";
            this.btnRequestIngredients.UseVisualStyleBackColor = false;
            this.btnRequestIngredients.Click += new System.EventHandler(this.btnRequestIngredients_Click);
            // 
            // buttonLogout
            // 
            this.buttonLogout.BackColor = System.Drawing.Color.PapayaWhip;
            this.buttonLogout.Font = new System.Drawing.Font("Papyrus", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonLogout.Location = new System.Drawing.Point(1107, 36);
            this.buttonLogout.Name = "buttonLogout";
            this.buttonLogout.Size = new System.Drawing.Size(145, 53);
            this.buttonLogout.TabIndex = 17;
            this.buttonLogout.Text = "Sign out";
            this.buttonLogout.UseVisualStyleBackColor = false;
            this.buttonLogout.Click += new System.EventHandler(this.buttonLogout_Click);
            // 
            // cookForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = global::ISS_Application.Properties.Resources.cookMenu1;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ClientSize = new System.Drawing.Size(1354, 733);
            this.Controls.Add(this.buttonLogout);
            this.Controls.Add(this.btnRequestIngredients);
            this.Controls.Add(this.btnViewIngredients);
            this.Controls.Add(this.labelTitle);
            this.Name = "cookForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "cookForm";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Label labelTitle;
        private System.Windows.Forms.Button btnViewIngredients;
        private System.Windows.Forms.Button btnRequestIngredients;
        private System.Windows.Forms.Button buttonLogout;
    }
}