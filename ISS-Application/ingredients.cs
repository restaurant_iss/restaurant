﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ISS_Application
{
    public partial class ingredients : Form
    {

        SqlConnection con = new SqlConnection(Program.connectionString);
        SqlDataAdapter da = new SqlDataAdapter();
        DataSet ds = new DataSet();
       // BindingSource bs = new BindingSource();

        public ingredients()
        {
            InitializeComponent();
        }


        private void btnBack_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void buttonShowIngredients_Click(object sender, EventArgs e)
        {
            ds.Clear();
            da.SelectCommand = new SqlCommand("SELECT * FROM Ingredient", con);
            da.Fill(ds, "Ingredient");
            dataGridView1.DataSource = ds.Tables["Ingredient"];
        }
    }
}
