﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

using System.Data.SqlClient;

namespace ISS_Application
{
    public partial class giveFeedback : Form
    {
        SqlConnection con = new SqlConnection(Program.connectionString);
        SqlDataAdapter da = new SqlDataAdapter();
        DataSet ds = new DataSet();

        public giveFeedback()
        {
            InitializeComponent();
        }

        private void giveFeedback_Load(object sender, EventArgs e)
        {
            pictureBox1.Hide();
        }

        private void buttonBack_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void buttonAddFeedback_Click(object sender, EventArgs e)
        {
            DateTime d = DateTime.Now;
            int rating = 0;
            if (radioButton1.Checked)
                rating = 1;
            else if (radioButton2.Checked)
                rating = 2;
            else if (radioButton3.Checked)
                rating = 3;
            else if (radioButton4.Checked)
                rating = 4;
            else if (radioButton5.Checked)
                rating = 5;

            String message = richTextBox1.Text.ToString();
            String signature = textBox1.Text.ToString();

            da.InsertCommand = new SqlCommand("Insert into Feedback(Date, Rating, Message, Signature) Values (@date, @rating, @message, @signature)", con);
            da.InsertCommand.Parameters.Add("@date", SqlDbType.Date).Value = d;
            da.InsertCommand.Parameters.Add("@rating", SqlDbType.Int).Value = int.Parse(rating.ToString());
            da.InsertCommand.Parameters.Add("@message", SqlDbType.VarChar).Value = message;
            da.InsertCommand.Parameters.Add("@signature", SqlDbType.VarChar).Value = signature;

            con.Open();
            da.InsertCommand.ExecuteNonQuery();
            con.Close();

            pictureBox1.Show();
        }
    }
}
