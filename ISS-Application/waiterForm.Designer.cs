﻿namespace ISS_Application
{
    partial class waiterForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(waiterForm));
            this.label1 = new System.Windows.Forms.Label();
            this.buttonTakeOrders = new System.Windows.Forms.Button();
            this.buttonCollectMoney = new System.Windows.Forms.Button();
            this.buttonLogout = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.Transparent;
            this.label1.Cursor = System.Windows.Forms.Cursors.Default;
            this.label1.Font = new System.Drawing.Font("Monotype Corsiva", 27.75F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))));
            this.label1.ForeColor = System.Drawing.Color.Sienna;
            this.label1.Location = new System.Drawing.Point(451, 61);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(452, 45);
            this.label1.TabIndex = 0;
            this.label1.Text = "Welcome to the waiter\'s menu!";
            // 
            // buttonTakeOrders
            // 
            this.buttonTakeOrders.BackColor = System.Drawing.Color.PapayaWhip;
            this.buttonTakeOrders.Font = new System.Drawing.Font("Papyrus", 11.25F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonTakeOrders.Location = new System.Drawing.Point(340, 431);
            this.buttonTakeOrders.Name = "buttonTakeOrders";
            this.buttonTakeOrders.Size = new System.Drawing.Size(142, 66);
            this.buttonTakeOrders.TabIndex = 1;
            this.buttonTakeOrders.Text = "Take Orders";
            this.buttonTakeOrders.UseVisualStyleBackColor = false;
            this.buttonTakeOrders.Click += new System.EventHandler(this.buttonTakeOrders_Click);
            // 
            // buttonCollectMoney
            // 
            this.buttonCollectMoney.BackColor = System.Drawing.Color.PapayaWhip;
            this.buttonCollectMoney.Font = new System.Drawing.Font("Papyrus", 11.25F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonCollectMoney.Location = new System.Drawing.Point(777, 431);
            this.buttonCollectMoney.Name = "buttonCollectMoney";
            this.buttonCollectMoney.Size = new System.Drawing.Size(153, 65);
            this.buttonCollectMoney.TabIndex = 2;
            this.buttonCollectMoney.Text = "Collect Money";
            this.buttonCollectMoney.UseVisualStyleBackColor = false;
            this.buttonCollectMoney.Click += new System.EventHandler(this.buttonCollectMoney_Click);
            // 
            // buttonLogout
            // 
            this.buttonLogout.BackColor = System.Drawing.Color.PapayaWhip;
            this.buttonLogout.Font = new System.Drawing.Font("Papyrus", 12F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonLogout.Location = new System.Drawing.Point(978, 31);
            this.buttonLogout.Name = "buttonLogout";
            this.buttonLogout.Size = new System.Drawing.Size(145, 53);
            this.buttonLogout.TabIndex = 3;
            this.buttonLogout.Text = "Sign out";
            this.buttonLogout.UseVisualStyleBackColor = false;
            this.buttonLogout.Click += new System.EventHandler(this.buttonLogout_Click);
            // 
            // waiterForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FloralWhite;
            this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ClientSize = new System.Drawing.Size(1354, 733);
            this.Controls.Add(this.buttonLogout);
            this.Controls.Add(this.buttonCollectMoney);
            this.Controls.Add(this.buttonTakeOrders);
            this.Controls.Add(this.label1);
            this.Location = new System.Drawing.Point(470, 94);
            this.Name = "waiterForm";
            this.Text = "Waiter Menu";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button buttonTakeOrders;
        private System.Windows.Forms.Button buttonCollectMoney;
        private System.Windows.Forms.Button buttonLogout;
    }
}