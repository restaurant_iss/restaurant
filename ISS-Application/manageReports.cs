﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;

namespace ISS_Application
{
    public partial class manageReports : Form
    {
        int current = 0,currentid;
        int end;
        SqlConnection con = new SqlConnection(Program.connectionString);
        SqlDataAdapter da = new SqlDataAdapter();
        DataSet ds = new DataSet();
        BindingSource bs = new BindingSource();
        public manageReports()
        {
            InitializeComponent();
        }

        private void populateDataSet()
        {
            con.Open();
            String selectCmd = "Select * from Feedback order By Date DESC";
            da.SelectCommand = new SqlCommand(selectCmd, con);
            da.Fill(ds, "Feedback");
            con.Close();
        }

        private void populateGridView()
        {
            bs.DataSource = ds;
            bs.DataMember = "Feedback";
            dgv.DataSource = bs;
        }

        private void manageReports_Load(object sender, EventArgs e)
        {
            btnPrev.Visible = false;
            ds.Clear();
            populateDataSet();
            populateGridView();
            end = dgv.RowCount - 1;
            loadAtRow(0);

        }

        private void btnBack_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnNext_Click(object sender, EventArgs e)
        {
            if (current < end - 1)
            {
                current++;
                btnPrev.Visible = true;
                loadAtRow(current);
            }
            if(current==end-1)
            {
                btnNext.Visible = false;
            }
        }

        private void btnPrev_Click(object sender, EventArgs e)
        {
            if (current > 0)
            {
                current--;
                btnNext.Visible = true;
                loadAtRow(current);
            }
            if (current == 0)
            {
                btnPrev.Visible = false;
            }
        }

        private void btnClear_Click(object sender, EventArgs e)
        {
            DialogResult result = MessageBox.Show("Are you sure you want to proceed?", "Confirmation", MessageBoxButtons.YesNo);
            if (result == DialogResult.No)
                return;
            da.DeleteCommand = new SqlCommand("Delete from Feedback where ID=@id", con);
            da.DeleteCommand.Parameters.Add("@id", SqlDbType.Int).Value = currentid;

            con.Open();
            da.DeleteCommand.ExecuteNonQuery();
            ds.Tables["Feedback"].Clear();
            da.SelectCommand = new SqlCommand("Select * from Feedback order By Date DESC", con);
            da.Fill(ds, "Feedback");
            con.Close();
            MessageBox.Show("Record was deleted!");
            end--;

            if (current == end)
                current--;
            if (current == end - 1)
                btnNext.Visible = false;
            

            loadAtRow(current);
        }

        private void loadAtRow(int row)
        {
            currentid = (int)dgv.Rows[row].Cells[0].Value;
            string date = dgv.Rows[row].Cells[1].Value.ToString().Trim();
            int rating = (int)dgv.Rows[row].Cells[2].Value;
            string message= dgv.Rows[row].Cells[3].Value.ToString().Trim();
            string signature= dgv.Rows[row].Cells[4].Value.ToString().Trim();

            txtDate.Text = date;
            txtMessage.Text = message;
            txtSignature.Text = signature;
            string appPath = System.IO.Path.GetDirectoryName(Application.ExecutablePath);
            switch (rating)
            {
                case 1:
                    pbFeedback.ImageLocation = appPath + @"\ratings\1.jpg";
            
                    break;
                case 2:
                    pbFeedback.ImageLocation = appPath + @"\ratings\2.jpg";
                    break;
                case 3:
                    pbFeedback.ImageLocation = appPath + @"\ratings\3.jpg";
                    break;
                case 4:
                    pbFeedback.ImageLocation = appPath + @"\ratings\4.jpg";
                    break;
                case 5:
                    pbFeedback.ImageLocation = appPath + @"\ratings\5.jpg";
                    break;
                default:
                    pbFeedback.Image = pbFeedback.InitialImage;// pbFeedback.InitialImage;
                    break;
            }


        }
    }
}
