﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;



namespace ISS_Application
{
    public partial class clientForm : Form
    {
        public clientForm()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            
        }

       
        private void buttonShowMenu_Click(object sender, EventArgs e)
        {
            showMenu showMenu = new showMenu();
            showMenu.Show(); 
        }

        private void buttonManageOrder_Click(object sender, EventArgs e)
        {
            order order = new order();
            order.Show();           
        }

        private void buttonGetFeedback_Click(object sender, EventArgs e)
        {
            feedback feedback = new feedback();
            feedback.Show();
        }

        private void buttonGiveFeedback_Click(object sender, EventArgs e)
        {
            giveFeedback giveFeedback = new giveFeedback();
            giveFeedback.Show();
        }

        private void Form1_FormClosed(object sender, FormClosedEventArgs e)
        {
            
        }

        private void buttonBack_Click(object sender, EventArgs e)
        {
            this.Hide();
            MainForm m = new MainForm();
            m.FormClosed += (s, a) => this.Close();
            m.Show();
        }
    }
}
