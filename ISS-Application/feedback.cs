﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

using System.Data.SqlClient;

namespace ISS_Application
{
    public partial class feedback : Form
    {
        SqlConnection con = new SqlConnection(Program.connectionString);
        SqlDataAdapter da = new SqlDataAdapter();
        DataSet ds = new DataSet();
        public feedback()
        {
            InitializeComponent();
        }

        private void feedback_Load(object sender, EventArgs e)
        {
            ds.Clear();
            da.SelectCommand = new SqlCommand("SELECT Signature, Message, Rating FROM Feedback", con);
            da.Fill(ds, "Feedback");
            dataGridView2.DataSource = ds.Tables["Feedback"];
            int nr_of_rows = dataGridView2.RowCount;

            int y = 50;
            Label[] l = new Label[3];
            for (int i = 0; i < 3; i++)
            {
                if (dataGridView2.Rows[nr_of_rows - i - 2].Cells[2].Value.ToString() != "")
                {
                    int nr_of_stars = int.Parse(dataGridView2.Rows[nr_of_rows - i - 2].Cells[2].Value.ToString());
                    PictureBox[] p = new PictureBox[nr_of_stars];
                    int x = 800;
                    for (int j = 0; j < nr_of_stars; j++)
                    {
                        Size size = new Size(50, 50);
                        p[j] = new PictureBox();
                        p[j].Location = new Point(x, y);
                        p[j].ImageLocation = "starr.jpg";
                        p[j].Size = size;
                        p[j].SizeMode = PictureBoxSizeMode.StretchImage;
                        this.Controls.Add(p[j]);
                        x += 50;
                    }
                }
                l[i] = new Label();
                l[i].Location = new Point(800, y+50);
                l[i].Text = dataGridView2.Rows[nr_of_rows - i - 2].Cells[1].Value.ToString().Trim();
                Size size2 = new Size(250, 50);
                l[i].Size = size2;
                this.Controls.Add(l[i]);
                y += 200;
            }
        }

        private void buttonBack_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void bindingSource1_CurrentChanged(object sender, EventArgs e)
        {

        }
    }
}
