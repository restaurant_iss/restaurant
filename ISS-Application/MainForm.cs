﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;

namespace ISS_Application
{
    public partial class MainForm : Form
    {
        SqlConnection connection = new SqlConnection(Program.connectionString);
        SqlDataAdapter da;
        DataSet ds = new DataSet();
        public String userType = "";

        public MainForm()
        {
            InitializeComponent();
        }

        private void btnClient_Click(object sender, EventArgs e)
        {
            this.userType = "client";
            clientForm cl = new clientForm();
            cl.FormClosed += (s, args) => this.Close();
            this.Hide();
            cl.Show();
        }

        private void btnLogin_Click(object sender, EventArgs e)
        {
            String username = textBoxUser.Text;
            String password = textBoxPassword.Text;
            
            if (username == "" || password == "")
            {
                MessageBox.Show("Please provide Username and Password");
                return;
            }
            try
            {
                
                SqlCommand cmd = new SqlCommand("Select * from UserLogIn where Username=@username", connection);
                cmd.Parameters.AddWithValue("@username", username);
                da = new SqlDataAdapter(cmd);
                da.Fill(ds, "user");
                int count = ds.Tables[0].Rows.Count;
                
                if (count == 1)
                {
                    if(ds.Tables["user"].Rows[0]["Password"].ToString().Equals(password))
                    {
                       //MessageBox.Show("Login Successful!");
                        this.userType = ds.Tables["user"].Rows[0]["UserType"].ToString();
                        switch (this.userType)
                        {
                            case "manager":
                                managerForm m = new managerForm();
                                m.FormClosed += (s, args) => this.Close();
                                this.Hide();
                                m.Show();
                                break;
                            case "cook":
                                cookForm c = new cookForm();
                                c.FormClosed += (s, args) => this.Close();
                                this.Hide();
                                c.Show();
                                break;
                            case "waiter":
                                waiterForm w = new waiterForm();
                                w.FormClosed += (s, args) => this.Close();
                                
                                this.Hide();
                                w.Show();
                                break;
                            default:
                                Application.Exit();
                                break;
                        }

                    }
                    else
                    {
                        MessageBox.Show("Password incorrect.");
                        ds.Clear();
                    }
                   
                }
                else
                {
                    MessageBox.Show("Username or password incorrect.");
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void MainForm_FormClosing(object sender, FormClosingEventArgs e)
        {
           
        }

        private void textBoxUser_TextChanged(object sender, EventArgs e)
        {

        }
    }
    
}
