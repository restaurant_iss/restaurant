﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;

namespace ISS_Application
{
    public partial class updateMenus : Form
    {
        SqlConnection con = new SqlConnection(Program.connectionString);
        SqlDataAdapter da = new SqlDataAdapter();
        DataSet ds = new DataSet();
        BindingSource bs = new BindingSource();
        public updateMenus()
        {
            InitializeComponent();
        }

        private void populateDataSet()
        {
            con.Open();
            String selectCmd = "Select * from Menu";
            da.SelectCommand = new SqlCommand(selectCmd, con);
            da.Fill(ds, "Menu");
            con.Close();
        }

        private void populateGridView()
        {
            bs.DataSource = ds;
            bs.DataMember = "Menu";
            dgv.DataSource = bs;
        }


        private void pbMenu_Click(object sender, EventArgs e)
        {

        }

        private void label8_Click(object sender, EventArgs e)
        {

        }

        private void updateMenus_Load(object sender, EventArgs e)
        {
            ds.Clear();
            populateDataSet();
            populateGridView();
        }

        private void btnBack_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void dgvEmployees_RowHeaderMouseClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            int id = (int)dgv.CurrentRow.Cells[0].Value;
            String title = dgv.CurrentRow.Cells[1].Value.ToString();
            String description = dgv.CurrentRow.Cells[2].Value.ToString();
            String menuimg = dgv.CurrentRow.Cells[3].Value.ToString();
            String category = dgv.CurrentRow.Cells[4].Value.ToString();
            String price = dgv.CurrentRow.Cells[5].Value.ToString();
            txtTitle.Text = title;
            txtDescription.Text = description;
            txtImgSource.Text = menuimg;
            txtCategory.Text = category;
            txtPrice.Text = price;

            string appPath = System.IO.Path.GetDirectoryName(Application.ExecutablePath);
            if (!menuimg.Contains(".jpg"))
                menuimg = "none.jpg";
            pbMenu.ImageLocation = menuimg;//appPath + @"\menus\" + menuimg;
            if (pbMenu.Image == pbMenu.ErrorImage)
            {
                pbMenu.Image = pbMenu.InitialImage;
                
            }
            pbMenu.SizeMode = PictureBoxSizeMode.Zoom;
            // MessageBox.Show(appPath + @"\profiles\"+profileimg);
        }

        private bool validateTexts()
        {
            if (txtTitle.Text == "" || txtCategory.Text == "" || txtPrice.Text == "")
            {
                MessageBox.Show("Some required field remained empty! Please fill them in.");
                return false;
            }
            int parsedValue;
            if (!int.TryParse(txtPrice.Text, out parsedValue))
            {
                MessageBox.Show("Salary is a number only field!");
                return false;
            }

            return true;
        }

        private void clearControls()
        {
            pbMenu.Image = null;
            foreach (Control c in gbDetails.Controls)
            {
                if (c is TextBox)
                {
                    ((TextBox)c).Text = "";
                }

            }
        }

        private void btnAdd_Click(object sender, EventArgs e)
        {
            String imgSrc = "";
            if (validateTexts() == false)
                return;
            if (txtImgSource.Text == "")
                imgSrc = "none.jpg";
            else
                imgSrc = txtImgSource.Text;
            da.InsertCommand = new SqlCommand("Insert into Menu(Title_Menu,Description_Menu,Photo_Menu,Category_Menu,Price) Values (@title,@desc,@photo,@category,@price)", con);
            da.InsertCommand.Parameters.Add("@title", SqlDbType.NChar).Value = txtTitle.Text;
            da.InsertCommand.Parameters.Add("@desc", SqlDbType.NChar).Value = txtDescription.Text;
            da.InsertCommand.Parameters.Add("@photo", SqlDbType.NChar).Value = imgSrc;
            da.InsertCommand.Parameters.Add("@category", SqlDbType.NChar).Value = txtCategory.Text;
            da.InsertCommand.Parameters.Add("@price", SqlDbType.Float).Value = txtPrice.Text;
            con.Open();
            da.InsertCommand.ExecuteNonQuery();
            ds.Tables["Menu"].Clear();
            da.SelectCommand = new SqlCommand("Select * from Menu", con);
            da.Fill(ds, "Menu");
            con.Close();
            MessageBox.Show("Menu item succesfully recorded!");
            clearControls();
        }

        private void btnClear_Click(object sender, EventArgs e)
        {
            clearControls();
        }

        private void btnUpdate_Click(object sender, EventArgs e)
        {
            if (validateTexts() == false)
                return;
            int id = (int)dgv.CurrentRow.Cells[0].Value;
            da.UpdateCommand = new SqlCommand("Update Menu Set Title_Menu=@title, Description_Menu=@desc, Photo_Menu=@photo, Category_Menu=@category, Price=@price Where ID_Menu=@id", con);
            da.UpdateCommand.Parameters.Add("@title", SqlDbType.NChar).Value = txtTitle.Text;
            da.UpdateCommand.Parameters.Add("@desc", SqlDbType.NChar).Value = txtDescription.Text;
            da.UpdateCommand.Parameters.Add("@photo", SqlDbType.NChar).Value = txtImgSource.Text;
            da.UpdateCommand.Parameters.Add("@category", SqlDbType.NChar).Value = txtCategory.Text;
            da.UpdateCommand.Parameters.Add("@price", SqlDbType.Float).Value = txtPrice.Text;
            da.UpdateCommand.Parameters.Add("@id", SqlDbType.Int).Value = id;
            con.Open();
            da.UpdateCommand.ExecuteNonQuery();
            ds.Tables["Menu"].Clear();
            da.SelectCommand = new SqlCommand("Select * from Menu", con);
            da.Fill(ds, "Menu");
            con.Close();
            MessageBox.Show("Menu item was updated!");
            clearControls();
        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            if (dgv.SelectedRows.Count == 0)
            {
                MessageBox.Show("No row selected!");
                return;
            }
            DialogResult result = MessageBox.Show("Are you sure you want to proceed?", "Confirmation", MessageBoxButtons.YesNo);
            if (result == DialogResult.No)
                return;
            int id = (int)dgv.CurrentRow.Cells[0].Value;
            da.DeleteCommand = new SqlCommand("Delete from Menu where ID_Menu=@id", con);
            da.DeleteCommand.Parameters.Add("@id", SqlDbType.Int).Value = id;

            con.Open();
            da.DeleteCommand.ExecuteNonQuery();
            ds.Tables["Menu"].Clear();
            da.SelectCommand = new SqlCommand("Select * from Menu", con);
            da.Fill(ds, "Menu");
            con.Close();
            MessageBox.Show("Menu item was removed!");
            clearControls();
        }
    }
}
