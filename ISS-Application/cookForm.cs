﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ISS_Application
{
    public partial class cookForm : Form
    {
        public cookForm()
        {
            InitializeComponent();
        }

        private void btnRequestIngredients_Click(object sender, EventArgs e)
        {
            requestIngredients requestIngrForm = new requestIngredients();
            requestIngrForm.Show();
        }

        private void btnViewIngredients_Click(object sender, EventArgs e)
        {
           ingredients IngrForm = new ingredients();
           IngrForm.Show();
        }

        private void btnBack_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void buttonLogout_Click(object sender, EventArgs e)
        {
            this.Hide();
            MainForm m = new MainForm();
            m.FormClosed += (s, a) => this.Close();
            m.Show();
        }
    }
}
