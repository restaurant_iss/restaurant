﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ISS_Application
{
    public partial class waiterForm : Form
    {
        public waiterForm()
        {
            InitializeComponent();
        }

        private void buttonCollectMoney_Click(object sender, EventArgs e)
        {
            collectMoney collectMoneyForm = new collectMoney();
            collectMoneyForm.Show();
        }

        private void buttonTakeOrders_Click(object sender, EventArgs e)
        {
            takeOrders takeOrdersForm = new takeOrders();
            takeOrdersForm.Show();
        }

        private void buttonLogout_Click(object sender, EventArgs e)
        {
            this.Hide();
            MainForm m = new MainForm();
            m.FormClosed += (s, a) => this.Close();
            m.Show();
        }
    }
}
