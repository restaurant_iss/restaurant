﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

using System.Data.SqlClient;

namespace ISS_Application
{
    public partial class order : Form
    {
        int startindex = 0;
        int endindex;
        SqlConnection con = new SqlConnection(Program.connectionString);
        SqlDataAdapter da = new SqlDataAdapter();
        DataSet ds = new DataSet();
        int tableAvailable;
        public order()
        {
            InitializeComponent();
        }

        private void showMenu_Load(object sender, EventArgs e)
        {
            if (startindex == 0)
            {
                button1.Visible = false;
            }
            ds.Clear();
            da.SelectCommand = new SqlCommand("SELECT * FROM Menu", con);
            da.Fill(ds, "Menu");
            dataGridView1.DataSource = ds.Tables["Menu"];

            endindex = dataGridView1.RowCount - 1;
            label2.Text = dataGridView1.Rows[startindex].Cells[1].Value.ToString().Trim();
            if (dataGridView1.Rows[startindex].Cells[2].Value.ToString() != "")
                label3.Text = dataGridView1.Rows[startindex].Cells[2].Value.ToString().Trim();
            else
                label3.Text = "No Description available";
            label4.Text = "$" + dataGridView1.Rows[startindex].Cells[5].Value.ToString().Trim();
            pictureBox1.ImageLocation = dataGridView1.Rows[startindex].Cells[3].Value.ToString().Trim();


            /* data grid view 2 */
            da.SelectCommand = new SqlCommand("SELECT TableNumber, Status FROM Orders", con);
            da.Fill(ds, "Orders");
            dataGridView2.DataSource = ds.Tables["Orders"];

            List<int> tables = new List<int>();
            for (int i=0; i<dataGridView2.Rows.Count - 1; i++)
            {
                tables.Add(int.Parse(dataGridView2.Rows[i].Cells[0].Value.ToString()));
            }
            List<int> list = new List<int>();
            for (int i = 1; i <= 15; i++)
                list.Add(i);

            List<int> listNew = new List<int>();
            listNew = list.Except(tables).ToList();
            tableAvailable = listNew[0];
            label5.Text += tableAvailable;
        }    

        private void buttonNext_Click(object sender, EventArgs e)
        {
            if (startindex < endindex - 1)
            {
                button1.Visible = true;
                startindex++;
                label2.Text = dataGridView1.Rows[startindex].Cells[1].Value.ToString().Trim();
                if (dataGridView1.Rows[startindex].Cells[2].Value.ToString() != "")
                    label3.Text = dataGridView1.Rows[startindex].Cells[2].Value.ToString().Trim();
                else
                    label3.Text = "No Description available";
                label4.Text ="$" + dataGridView1.Rows[startindex].Cells[5].Value.ToString().Trim();
                pictureBox1.ImageLocation = dataGridView1.Rows[startindex].Cells[3].Value.ToString().Trim();
            }
            if (startindex == endindex - 1)
            {
                button2.Visible = false;
            }
        }

        private void buttonBack_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void buttonPrev_Click(object sender, EventArgs e)
        {
            if (startindex > 0)
            {
                button2.Visible = true;
                startindex--;
                label2.Text = dataGridView1.Rows[startindex].Cells[1].Value.ToString().Trim();
                if (dataGridView1.Rows[startindex].Cells[2].Value.ToString() != "")
                    label3.Text = dataGridView1.Rows[startindex].Cells[2].Value.ToString().Trim();
                else
                    label3.Text = "No Description available";
                label4.Text = "$" + dataGridView1.Rows[startindex].Cells[5].Value.ToString().Trim();
                pictureBox1.ImageLocation = dataGridView1.Rows[startindex].Cells[3].Value.ToString().Trim();
            }
            if (startindex == 0)
            {
                button1.Visible = false;
            }
        }

        private void buttonAddToOrder_Click(object sender, EventArgs e)
        {
            int id_menu = int.Parse(dataGridView1.Rows[startindex].Cells[0].Value.ToString());

            da.InsertCommand = new SqlCommand("Insert into Orders(ID_Menu, TableNumber, WaiterID, Status, Takeout, PayMethod, Pay_Request) Values (@id_menu, @tableNumber, @waiterId, @status, @takeout, @paymethod, @pay_request)", con);
            da.InsertCommand.Parameters.Add("@id_menu", SqlDbType.Int).Value = id_menu;

            Random rnd = new Random();
            int waiterId = rnd.Next(1, 3);

            String takeout;

            if (checkBox2.Checked)
            {
                takeout = "yes";
            }
            else
                takeout = "no";

            String paymethod;
            if (radioButton1.Checked == true)
            {
                paymethod = "cash";
             }
            else
                paymethod = "card";

            da.InsertCommand.Parameters.Add("@tableNumber", SqlDbType.Int).Value = tableAvailable;
            da.InsertCommand.Parameters.Add("@waiterId", SqlDbType.Int).Value = waiterId;
            da.InsertCommand.Parameters.Add("@status", SqlDbType.VarChar).Value = "pending";
            da.InsertCommand.Parameters.Add("@takeout", SqlDbType.VarChar).Value = takeout;
            da.InsertCommand.Parameters.Add("@paymethod", SqlDbType.VarChar).Value = paymethod;
            da.InsertCommand.Parameters.Add("@pay_request", SqlDbType.Bit).Value = 1;

            con.Open();
            da.InsertCommand.ExecuteNonQuery();
            con.Close();
        }

        private void buttonFinishOrder_Click(object sender, EventArgs e)
        {
            da.SelectCommand = new SqlCommand("SELECT Title_Menu FROM Menu JOIN Orders ON Orders.ID_Menu = Menu.ID_Menu WHERE TableNumber = @tablenumber", con);
            da.SelectCommand.Parameters.Add("@tableNumber", SqlDbType.Int).Value = tableAvailable;

            ds.Clear();
            da.Fill(ds, "Menu");
            dataGridView3.DataSource = ds.Tables["Menu"];

            String s = "This is what you ordered:\n";

            for (int i = 0; i < dataGridView3.Rows.Count - 1; i++)
            {
                s += "\t" + dataGridView3.Rows[i].Cells[1].Value.ToString() + "\n";
            }

            MessageBox.Show(s);
        }
    }
}
