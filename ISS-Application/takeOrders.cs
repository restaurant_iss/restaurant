﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ISS_Application
{
    public partial class takeOrders : Form
    {
        SqlConnection connection = new SqlConnection(Program.connectionString);
        SqlDataAdapter da = new SqlDataAdapter();
        DataSet ds = new DataSet();
        BindingSource bs = new BindingSource();

        public takeOrders()
        {
            InitializeComponent();
        }

        private void takeOrders_Load(object sender, EventArgs e)
        {
            connection.Open();
            da.SelectCommand = new SqlCommand(@"SELECT * FROM Orders o
                                                inner join Menu m on o.ID_Menu = m.ID_Menu
                                                WHERE Status = 'pending'", connection);
            da.Fill(ds, "AvailableOrders");
            bs.DataSource = ds;
            bs.DataMember = "AvailableOrders";

            OrdersGridView.AutoGenerateColumns = false;
            OrdersGridView.ColumnCount = 6;
            OrdersGridView.ReadOnly = true;

            OrdersGridView.Columns[0].Name = "Table";
            OrdersGridView.Columns[0].DataPropertyName = "TableNumber";

            OrdersGridView.Columns[1].Name = "Menu";
            OrdersGridView.Columns[1].DataPropertyName = "Title_Menu";

            OrdersGridView.Columns[2].Name = "Price";
            OrdersGridView.Columns[2].DataPropertyName = "Price";

            OrdersGridView.Columns[3].Name = "Takeout";
            OrdersGridView.Columns[3].DataPropertyName = "Takeout";

            OrdersGridView.Columns[4].Name = "PayMethod";
            OrdersGridView.Columns[4].DataPropertyName = "PayMethod";

            OrdersGridView.Columns[5].Name = "ID_Order";
            OrdersGridView.Columns[5].DataPropertyName = "ID_Order";
            OrdersGridView.Columns[5].Visible = false;

            OrdersGridView.DataSource = bs;
            connection.Close();
        }

        private void BackButton_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void SaveButton_Click(object sender, EventArgs e)
        {
            connection.Open();
            SqlCommand cmd;
            foreach (DataGridViewRow r in OrdersGridView.SelectedRows)
            {
                int id;
                int.TryParse(r.Cells["ID_Order"].Value.ToString(), out id);
                cmd = new SqlCommand("UPDATE Orders SET Status = 'in process' WHERE ID_Order = @id");
                cmd.CommandType = CommandType.Text;
                cmd.Parameters.Add("@id", SqlDbType.Int).Value = id;
                cmd.Connection = connection;
                cmd.ExecuteNonQuery();
            }

            ds.Tables["AvailableOrders"].Clear();
            da.Fill(ds, "AvailableOrders");
            OrdersGridView.Update();
            OrdersGridView.Refresh();
            connection.Close();
        }
    }
}
