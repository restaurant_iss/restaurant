﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ISS_Application
{
    public partial class requestIngredients : Form
    {
        SqlConnection con = new SqlConnection(Program.connectionString);
        SqlDataAdapter da = new SqlDataAdapter();
        DataSet ds = new DataSet();
        BindingSource bs = new BindingSource();

        public requestIngredients()
        {
            InitializeComponent();
        }

        private void populateDataSet()
        {
            con.Open();
            String selectCmd = "SELECT Name FROM Ingredient WHERE QuantityStatus=1";
            da.SelectCommand = new SqlCommand(selectCmd, con);
            da.Fill(ds, "Ingredient");
            con.Close();
        }

        private void populateGridView()
        {
            bs.DataSource = ds;
            bs.DataMember = "Ingredient";
            missingGV.DataSource = bs;
        }


        private void btnBack_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void requestIngredients_Load(object sender, EventArgs e)
        {
            ds.Clear();
            populateDataSet();
            populateGridView();
        }

        private void btnRequest_Click(object sender, EventArgs e)
        {
            con.Open(); 
            String selectedIngr = missingGV.CurrentRow.Cells[0].Value.ToString();

            SqlCommand comanda = new SqlCommand();
            comanda.Connection = con;
            comanda.CommandType = CommandType.Text;
            comanda.CommandText = "UPDATE Ingredient SET QuantityStatus = 0 WHERE Name=@selectedIngr";
            comanda.Parameters.AddWithValue("@selectedIngr", selectedIngr);
            comanda.ExecuteNonQuery();

            ds.Tables["Ingredient"].Clear();
            String selectCmd = "SELECT Name FROM Ingredient WHERE QuantityStatus=1";
            da.SelectCommand = new SqlCommand(selectCmd, con);
            da.Fill(ds, "Ingredient");
       
            con.Close();
            MessageBox.Show("Request  submitted");
        }

    }       
}
