﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;


namespace ISS_Application
{
    public partial class manageStaff : Form
    {
        SqlConnection con = new SqlConnection(Program.connectionString);
        SqlDataAdapter da = new SqlDataAdapter();
        DataSet ds = new DataSet();
        BindingSource bs = new BindingSource();

         private void populateDataSet()
        {
            con.Open();
            String selectCmd = "Select * from Employees";
            da.SelectCommand = new SqlCommand(selectCmd, con);
            da.Fill(ds, "Employees"); 
            con.Close();
        }

        private void populateGridView()
        {
            bs.DataSource = ds;
            bs.DataMember = "Employees";
            dgvEmployees.DataSource = bs;
        }



        public manageStaff()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void manageStaff_Load(object sender, EventArgs e)
        {
            ds.Clear();
            populateDataSet();
            populateGridView();
        }

        private void gbDetails_Enter(object sender, EventArgs e)
        {

        }

        private void dgvEmployees_RowHeaderMouseClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            int id = (int)dgvEmployees.CurrentRow.Cells[0].Value;
            String name = dgvEmployees.CurrentRow.Cells[1].Value.ToString();
            String surname = dgvEmployees.CurrentRow.Cells[2].Value.ToString();
            String birthdate = dgvEmployees.CurrentRow.Cells[3].Value.ToString();
            String cnp = dgvEmployees.CurrentRow.Cells[4].Value.ToString();
            String address = dgvEmployees.CurrentRow.Cells[5].Value.ToString();
            String employmentdate = dgvEmployees.CurrentRow.Cells[6].Value.ToString();
            String position = dgvEmployees.CurrentRow.Cells[7].Value.ToString();
            String salary = dgvEmployees.CurrentRow.Cells[8].Value.ToString();
            String obs = dgvEmployees.CurrentRow.Cells[9].Value.ToString();
            String profileimg = dgvEmployees.CurrentRow.Cells[10].Value.ToString();
            txtName.Text = name;
            txtSurname.Text = surname;
            txtBirthDate.Text = birthdate;
            txtCNP.Text = cnp;
            txtAddress.Text = address;
            txtEmpl.Text = employmentdate;
            txtPosition.Text = position;
            txtSalary.Text = salary;
            txtObservations.Text = obs;
            txtImgSource.Text = profileimg;
            string appPath = System.IO.Path.GetDirectoryName(Application.ExecutablePath);
            if (!profileimg.Contains(".jpg"))
                profileimg = "none.jpg";
            pbProfile.ImageLocation = appPath + @"\profiles\"+profileimg;
            if(pbProfile.Image==pbProfile.ErrorImage)
            {
                pbProfile.Image = pbProfile.InitialImage;
            }
           // MessageBox.Show(appPath + @"\profiles\"+profileimg);
        }

        private bool validateTexts()
        {
            if (txtName.Text == "" || txtSurname.Text == "" || txtCNP.Text == "" || txtBirthDate.Text == "" || txtAddress.Text == "" || txtEmpl.Text == "" || txtPosition.Text=="" || txtSalary.Text=="" )
            {
                MessageBox.Show("Some required field remained empty! Please fill them in.");
                return false;
            }
            if (!txtBirthDate.Text.Contains("-") || !txtEmpl.Text.Contains("-"))
            {
                MessageBox.Show("Date format must be: YYYY-MM-DD");
                return false;
            }
            int parsedValue;
            if (!int.TryParse(txtSalary.Text, out parsedValue))
            {
                MessageBox.Show("Salary is a number only field!");
                return false;
            }

            return true;
        }
        private void clearControls()
        {
            pbProfile.Image = null; 
            foreach (Control c in gbDetails.Controls)
            {
                if(c is TextBox)
                {
                    ((TextBox)c).Text = "";
                }
        
            }
        }
        private void btnHire_Click(object sender, EventArgs e)
        {

            if (validateTexts() == false)
                return;
            da.InsertCommand = new SqlCommand("Insert into Employees(Name,Surname,Birthdate,CNP,Address ,EmploymentDate,Position,Salary,Observations,ProfileImg) Values (@name,@surname,@birthDate,@cnp,@address,@emplDate,@position,@salary,@obs,@profileImg)", con);
            da.InsertCommand.Parameters.Add("@name", SqlDbType.NChar).Value = txtName.Text;
            da.InsertCommand.Parameters.Add("@surname", SqlDbType.NChar).Value = txtSurname.Text;
            da.InsertCommand.Parameters.Add("@birthDate", SqlDbType.Date).Value = txtBirthDate.Text;
            da.InsertCommand.Parameters.Add("@cnp", SqlDbType.NChar).Value = txtCNP.Text;
            da.InsertCommand.Parameters.Add("@address", SqlDbType.NChar).Value = txtAddress.Text;
            da.InsertCommand.Parameters.Add("@emplDate", SqlDbType.Date).Value = txtEmpl.Text;
            da.InsertCommand.Parameters.Add("@position", SqlDbType.NChar).Value = txtPosition.Text;
            da.InsertCommand.Parameters.Add("@salary", SqlDbType.Float).Value = txtSalary.Text;
            da.InsertCommand.Parameters.Add("@obs", SqlDbType.NChar).Value = txtObservations.Text;
            da.InsertCommand.Parameters.Add("@profileImg", SqlDbType.NChar).Value = txtImgSource.Text;
            con.Open();
            da.InsertCommand.ExecuteNonQuery();
            ds.Tables["Employees"].Clear();
            da.SelectCommand = new SqlCommand("Select * from Employees", con);
            da.Fill(ds, "Employees");
            con.Close();
            MessageBox.Show("Employee Data was succesfully recorded!");
            clearControls();
            //dgvEmployees.Refresh();
        }

        private void btnClear_Click(object sender, EventArgs e)
        {
            clearControls();
        }

        private void btnUpdate_Click(object sender, EventArgs e)
        {
            if( validateTexts()==false)
                return;
            int id = (int)dgvEmployees.CurrentRow.Cells[0].Value;

            da.UpdateCommand = new SqlCommand("Update Employees SET Name=@name, Surname=@surname, Birthdate=@birthdate, CNP=@cnp, Address=@address, EmploymentDate=@emplDate, Position=@position, Salary=@salary, Observations=@obs, ProfileImg=@profileImg Where ID=@id", con);
            da.UpdateCommand.Parameters.Add("@name", SqlDbType.NChar).Value = txtName.Text;
            da.UpdateCommand.Parameters.Add("@surname", SqlDbType.NChar).Value = txtSurname.Text;
            da.UpdateCommand.Parameters.Add("@birthDate", SqlDbType.Date).Value = txtBirthDate.Text;
            da.UpdateCommand.Parameters.Add("@cnp", SqlDbType.NChar).Value = txtCNP.Text;
            da.UpdateCommand.Parameters.Add("@address", SqlDbType.NChar).Value = txtAddress.Text;
            da.UpdateCommand.Parameters.Add("@emplDate", SqlDbType.Date).Value = txtEmpl.Text;
            da.UpdateCommand.Parameters.Add("@position", SqlDbType.NChar).Value = txtPosition.Text;
            da.UpdateCommand.Parameters.Add("@salary", SqlDbType.Float).Value = txtSalary.Text;
            da.UpdateCommand.Parameters.Add("@obs", SqlDbType.NChar).Value = txtObservations.Text;
            da.UpdateCommand.Parameters.Add("@profileImg", SqlDbType.NChar).Value = txtImgSource.Text;
            da.UpdateCommand.Parameters.Add("@id", SqlDbType.Int).Value = id;

            con.Open();
            da.UpdateCommand.ExecuteNonQuery();
            ds.Tables["Employees"].Clear();
            da.SelectCommand = new SqlCommand("Select * from Employees", con);
            da.Fill(ds, "Employees");
            con.Close();
            MessageBox.Show("Employee data was updated!");
            clearControls();

        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            if (dgvEmployees.SelectedRows.Count == 0)
            {
                MessageBox.Show("No row selected!");
                return;
            }
            DialogResult result = MessageBox.Show("Are you sure you want to proceed?", "Confirmation", MessageBoxButtons.YesNo);
            if (result == DialogResult.No)
                return;
            int id = (int)dgvEmployees.CurrentRow.Cells[0].Value;
            da.DeleteCommand = new SqlCommand("Delete from Employees where ID=@id", con);
            da.DeleteCommand.Parameters.Add("@id", SqlDbType.Int).Value = id;

            con.Open();
            da.DeleteCommand.ExecuteNonQuery();
            ds.Tables["Employees"].Clear();
            da.SelectCommand = new SqlCommand("Select * from Employees", con);
            da.Fill(ds, "Employees");
            con.Close();
            MessageBox.Show("Employee was fired!");
            clearControls();
        }

        private void btnBack_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
