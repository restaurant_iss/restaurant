Waiter:
	|---> Take Orders
		|---> change status for an order
		|---> see all orders and their status

	|---> Collect money 
		|---> see payment orders (made by clients)
		|---> compute total sum for selected orders
		|---> manage payment
			|---> validate received payment
			|---> compute change	
			|---> mark order as finished

	|---> Book a table
		|---> see all available tables from a certain date
		|---> book a table

Client:
	|---> see all available menus
	|---> manage an order 
		|---> see details for each menu
		|---> ask for a takeout	
		|---> choose payment method
	|---> feedback the services of the restaurant
	|---> ask for bill

Cook:
	|---> see all available ingriedients
	|---> ask for supplies
	|---> update status of ingredients

Manager:
	|---> hire/fire staff
		|---> crud operations 
	|---> see consumers' feedback
	|---> update menus
	|---> modify ingredients


	